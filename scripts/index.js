const wf = document.querySelector('.weather-forecast');
const wfList = document.querySelector('.wf__range');
let wfListItemsCount = 0;

// Current slider transform value
let sliderPosition = 0;
let startPosition = 0;

// Value of days slide in visible screen
const daysInView = 4;

getWeatherData().then(data => {
  if (data && data.length) {
    data.forEach(day => {
      wfList.appendChild(createDayElement(day));
    })
  }
})
.then(() => initSlider());


const initSlider = () => {
  const currentDay = wfList.querySelector('.wf__current-day');

  wfListItemsCount = wfList.children.length;
  sliderPosition = startPosition = currentDay.offsetLeft;

  slideBy(sliderPosition);


  // Это в угоду красоте. Ждем пока пройдет анимация слайда и только потом начинаем отображать наш
  // блок
  setTimeout(() => { wf.style.opacity = "1"; }, 500)
};


const prevDay = () => {
  sliderPosition -= 135;

  if (sliderPosition < 0)  {
    sliderPosition = startPosition;
  }

  slideBy(sliderPosition);
};


const nextDay = () => {
  const maxTransformValue = wfListItemsCount * 135 - (135 * daysInView);

  sliderPosition += 135;

  if (sliderPosition > maxTransformValue)  {
    sliderPosition = startPosition;
  }

  slideBy(sliderPosition);
};


const slideBy = pos => {
  wfList.style.transform = `translateX(-${pos}px)`;
};
