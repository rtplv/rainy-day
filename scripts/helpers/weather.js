/**
 * Months in ru locale
 * @type {string[]}
 */
const monthsMap = [
  'Января',
  'Февраля',
  'Марта',
  'Апреля',
  'Мая',
  'Июня',
  'Июля',
  'Августа',
  'Сентября',
  'Октября',
  'Ноября',
  'Декабря',
];
/**
 * Map for day of the week in ru locale
 *  * @type {string[]}
 */
const dotwMap = [
  'Воскресенье',
  'Понедельник',
  'Вторник',
  'Среда',
  'Четверг',
  'Пятница',
  'Суббота',
];

const getDotwByNum = dayNum => dotwMap[dayNum];

const getMonthByNum = num => monthsMap[num];

/**
 * Receive timestamp and return shape: { dotw, title }
 * @param timestamp
 */

const getWeatherDate = timestamp => {
  const currentDate = new Date();
  const dateObj = new Date(timestamp * 1000);

  const date = dateObj.getDate();
  const monthNum = dateObj.getMonth();

  const dotwNum = dateObj.getDay();

  let weatherDate = {
    dotw: getDotwByNum(dotwNum),
    title: `${date} ${getMonthByNum(monthNum)}`
  };

  if (currentDate.getDate() === dateObj.getDate() &&
      currentDate.getMonth() === dateObj.getMonth() && 
      currentDate.getFullYear() === dateObj.getFullYear()) {
    weatherDate.dotw = 'Сегодня';
    weatherDate.today = true;
  }

  return weatherDate;
};

/**
 * Receive weather params and return readable data
 * @param params
 */

const getWeatherMeta = params => {
  const { cloudiness, snow, rain } = params;
  const iconsPath = 'assets/images/wf-icons';

  if (!cloudiness) {
    return {
      iconSrc: `${iconsPath}/day-sunny.svg`,
      descr: 'Солнечно',
    }
  }

  if (cloudiness && !snow && !rain) {
    return {
      iconSrc: `${iconsPath}/cloud.svg`,
      descr: 'Облачно',
    }
  }

  if (snow && rain) {
    return {
      iconSrc: `${iconsPath}/rain-mix.svg`,
      descr: 'Дождь со снегом',
    }
  }

  if (snow && !rain) {
    return {
      iconSrc: `${iconsPath}/snow.svg`,
      descr: 'Снег',
    }
  }

  if (!snow && rain) {
    // Дождь
    return {
      iconSrc: `${iconsPath}/rain.svg`,
      descr: 'Дождь',
    }
  }
};
