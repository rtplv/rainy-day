/**
 * Create list item from received data.
 * @param data
 */
const createDayElement = data => {
  const {
    date,
    temperature: { day: dayTemp, night: nightTemp },
    cloudiness,
    snow,
    rain
  } = data;

  const weatherDate = getWeatherDate(date);
  const weatherMeta = getWeatherMeta({ cloudiness, snow, rain });


  const tmpl = `
    <div class="day__date">
      <div class="date__dotw">
          ${weatherDate.dotw}
      </div>
      <div class="date__title">
          ${weatherDate.title}
      </div>
    </div>
    <div class="day__precipitation">
      <img  class="precipitation__icon"
            src="${weatherMeta.iconSrc}"
            alt="${weatherMeta.descr}">
      <div class="precipitation__descr">
          ${weatherMeta.descr}
      </div>
    </div>

    <div class="day__temperature">
      <div class="temperature__day">
          днем ${dayTemp}°
      </div>
      <div class="temperature__night">
          ночью ${nightTemp}°
      </div>
    </div>
  `;

  const li = document.createElement('li');
        li.className = 'wf__day';
        li.innerHTML = tmpl;

        if (weatherDate.today) {
          li.className += ' wf__current-day';
        }

  return li;
};
