const getWeatherData = async () => {
  try {
    const response = await fetch('mocks/weather.json');
    return await response.json();
  } catch (error) {
    throw error;
  }
};
